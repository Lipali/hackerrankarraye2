import java.util.*;

public class Solution {

    public static boolean canWin(int leap, int[] game) {
        // Return true if you can win the game; otherwise, return false.

        int position=0;
        if(game[0]!=0){
            return false;
        }
        else if (leap+position > game.length){
            return true;
        }
        else {
            do {
                if (game[position + leap] == 0) {
                    position = position + leap-1;}
                else if (game[position + 1] == 0) {
                    position++;
                }
                 else
                    return false;
            }
            while (position + leap < game.length);
            return true;
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int q = scan.nextInt();
        while (q-- > 0) {
            int n = scan.nextInt();
            int leap = scan.nextInt();

            int[] game = new int[n];
            for (int i = 0; i < n; i++) {
                game[i] = scan.nextInt();
            }

            System.out.println( (canWin(leap, game)) ? "YES" : "NO" );
        }
        scan.close();
    }
}
